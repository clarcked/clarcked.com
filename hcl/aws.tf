provider "aws" {
  profile = "clarcked"
  region = "us-east-1"
}
resource "aws_instance" "clarcked-blog-instance" {
  ami = "ami-0ee02acd56a52998e"
  instance_type = "t1.micro"
  key_name = "clarcked"
  security_groups = [
    aws_security_group.allow_ssh.id,
    aws_security_group.allow_http.id,
    aws_security_group.allow_https.id
  ]
  subnet_id = aws_subnet.clarcked-blog-subnet.id
  root_block_device {
    volume_size           = "10"
    volume_type           = "gp2"
    encrypted             = false
    delete_on_termination = false
    tags = {
      Project = "Blog"
    }
  }
  tags = {
    Project = "Blog"
  }
}
