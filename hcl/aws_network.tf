resource "aws_vpc" "clarcked-blog-vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Project = "Blog"
  }
}
resource "aws_subnet" "clarcked-blog-subnet" {
  cidr_block = cidrsubnet(aws_vpc.clarcked-blog-vpc.cidr_block, 3, 1)
  vpc_id = aws_vpc.clarcked-blog-vpc.id
  availability_zone = "us-east-1a"
  tags = {
    Project = "Blog"
  }
}
resource "aws_eip" "clarcked-blog-ip" {
  instance = aws_instance.clarcked-blog-instance.id
  vpc      = true
  tags = {
    Project = "Blog"
  }
}
resource "aws_internet_gateway" "clarcked-blog-gw" {
  vpc_id = aws_vpc.clarcked-blog-vpc.id
  tags = {
    Project = "Blog"
  }
}
resource "aws_route_table" "clarcked-blog-route-table" {
  vpc_id = aws_vpc.clarcked-blog-vpc.id
  tags = {
    Project = "Blog"
  }
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.clarcked-blog-gw.id
  }
}
resource "aws_route_table_association" "clarcked-blog-subnet-association" {
  subnet_id      = aws_subnet.clarcked-blog-subnet.id
  route_table_id = aws_route_table.clarcked-blog-route-table.id
}