#!/bin/bash

sudo rm -rf ./bitnami/wordpress/*
sudo rm -rf ./db/data/*
sudo chmod -R 777 bitnami db certs
docker stop wordpress mariadb
docker system prune -a && \
docker-compose up --build -d --remove-orphans && \
docker ps && \
#
#docker run -it --rm --name certbot \
#            -v "$(pwd)/certs/etc/letsencrypt:/etc/letsencrypt" \
#            -v "$(pwd)/certs/var/lib/letsencrypt:/var/lib/letsencrypt" \
#            certbot/certbot certonly

echo "successfully installed..."