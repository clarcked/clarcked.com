#!/bin/bash

#sudo openssl genrsa -out certs/server.key 2048
#sudo openssl req -new -key certs/server.key -out certs/cert.csr

curl -Ls https://api.github.com/repos/xenolf/lego/releases/latest | grep browser_download_url | grep linux_amd64 | cut -d '"' -f 4 | wget -i -
tar xf lego_v4.4.0_linux_amd64.tar.gz
sudo mv lego /usr/local/bin/lego
sudo lego --tls --email="clarcked@outlook.com.ar" --domains="clarcked.com" --domains="www.clarcked.com" --path="certs/" --accept-tos run