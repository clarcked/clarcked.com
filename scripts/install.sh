#!/bin/bash

sudo rm -rf ./bitnami/wordpress/*
sudo rm -rf ./db/data/*
sudo chmod -R 777 bitnami db certs
sudo systemctl stop apache2

docker stop wordpress mariadb
docker system prune -a && \
docker-compose up --build -d --remove-orphans && \
docker ps && \
echo "successfully installed..."